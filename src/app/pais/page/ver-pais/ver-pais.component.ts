import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Country } from '../../inteface/pais.inteface';
import { PaisService } from '../../service/pais.service';
import { switchMap,tap } from 'rxjs';

@Component({
  selector: 'app-ver-pais',
  templateUrl: './ver-pais.component.html',
  styleUrls: ['./ver-pais.component.css']
})
export class VerPaisComponent implements OnInit{

  pais!: Country;
  name: String="";

  constructor(private paisService:PaisService,private activatedRouter: ActivatedRoute){}

  ngOnInit(): void {
    
    this.activatedRouter.params
    .pipe(
      switchMap(({id})=>this.paisService.getPaisCodigo(id)),tap(resp=>console.log(resp))
    )
    .subscribe(pais=>{this.pais=pais[0];});

    
  }
}
