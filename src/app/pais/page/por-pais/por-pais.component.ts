import { Component } from '@angular/core';
import { Country } from '../../inteface/pais.inteface';
import { PaisService } from '../../service/pais.service';

@Component({
  selector: 'app-por-pais',
  templateUrl: './por-pais.component.html',
  styleUrls: ['./por-pais.component.css']
})
export class PorPaisComponent {

  termino: string = "";
  Error: boolean = false;
  paises: Country[] = [];
  paisesSugeridos: Country[] = [];

  constructor(private paisService:PaisService){}

  buscar(termino:string){
    this.Error = false;
    this.termino = termino;
    this.paisService.buscarPorPais(this.termino).subscribe(paises=>{
      this.paises = paises;
      console.log()
    },(err)=>{
      this.Error = true;
      this.paises= [];
    })
  }

  sugerencias(termino:string){
    this.Error=false;
    this.paisService.buscarPorPais(termino)
      .subscribe(paises=>this.paisesSugeridos=paises.splice(0,3),
      (err)=>this.paisesSugeridos = []
    )
    
  }

}
