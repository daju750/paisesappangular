import { Component } from '@angular/core';
import { Country } from '../../inteface/pais.inteface';
import { PaisService } from '../../service/pais.service';

@Component({
  selector: 'app-por-region',
  templateUrl: './por-region.component.html',
  styleUrls: ['./por-region.component.css']
})
export class PorRegionComponent {

  regiones: string[] = ['africa','americas','asia','europe','oceania'];
  regionActiva: string= '';

  constructor(private paisService:PaisService){}

  activarRegion(region: string){

    this.regionActiva=region;

    this.paisService.buscarPorRegion(region).subscribe(paises=>{
      this.paises = paises;
    },(err)=>{
      this.paises= [];
    })
  }

  paises: Country[] = [];

}
