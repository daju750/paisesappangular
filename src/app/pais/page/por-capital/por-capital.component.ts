import { Component } from '@angular/core';
import { Country } from '../../inteface/pais.inteface';
import { PaisService } from '../../service/pais.service';

@Component({
  selector: 'app-por-capital',
  templateUrl: './por-capital.component.html',
  styleUrls: ['./por-capital.component.css']
})
export class PorCapitalComponent {
  termino: string = "";
  Error: boolean = false;
  paises: Country[] = [];

  constructor(private paisService:PaisService){}

  buscar(termino:string){
    this.Error = false;
    this.termino = termino;
    this.paisService.buscarPorCapital(this.termino).subscribe(paises=>{
      this.paises = paises;
      console.log(this.paises)
    },(err)=>{
      this.Error = true;
      this.paises= [];
    })
  }

  sugerencias(termino:string){
    this.Error=false;
  }


}
