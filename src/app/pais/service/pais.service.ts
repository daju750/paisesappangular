import {Injectable} from '@angular/core'
import {Observable} from 'rxjs'
import {HttpClient} from '@angular/common/http';
import { Country } from '../inteface/pais.inteface';

@Injectable({
    providedIn: 'root'
})

export class PaisService{
    
    private apiUrl:string = "https://restcountries.com/v3.1";
    private params:string ="?name,flags,population";
    constructor(private http: HttpClient){};

    buscarPorPais(termino:String): Observable<Country[]>{
        const url = this.apiUrl+"/name/"+termino+this.params;
        return this.http.get<Country[]>(url);
    }

    buscarPorCapital(termino:String): Observable<Country[]>{
        const url = this.apiUrl+"/capital/"+termino+this.params;
        return this.http.get<Country[]>(url);
    }

    buscarPorRegion(termino:String): Observable<Country[]>{
        const url = this.apiUrl+"/region/"+termino+this.params;
        return this.http.get<Country[]>(url);
    }

    getPaisCodigo(id:String): Observable<Country[]>{
        const url = this.apiUrl+"/alpha/"+id;
        return this.http.get<Country[]>(url);
    }

}